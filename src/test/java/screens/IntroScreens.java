package screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import configurations.AppFactory;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class IntroScreens extends BasePage{

	
	public IntroScreens() throws Exception {
		super();
		// TODO Auto-generated constructor stub
		 Reporter.log("Application started succesfully");
	}

	
	@AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"btnContinue, \"]")
    public WebElement btn_continue;
	
	@AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"btnStartShopping, \"]")
    public WebElement start_shoping;
	
//	@AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"btnContinue, \"]")
//    public WebElement continue_shoping;
	
	@AndroidFindBy(xpath = "(//android.view.ViewGroup[@content-desc=\"toMenuItem, \"])[1]/android.widget.TextView")
    public WebElement select_dept;

	WebDriverWait wait = new WebDriverWait(AppFactory.driver, 10);
	
    public void continueButton() throws Exception{
    	wait.until(ExpectedConditions.elementToBeClickable(btn_continue));
    	btn_continue.click();
    	Thread.sleep(3000);
    	Reporter.log("Introduction continue button clicked succefully");
    }
    
    public void startShopingButton() throws Exception{
    	wait.until(ExpectedConditions.elementToBeClickable(start_shoping));
    	start_shoping.click();
    	Thread.sleep(3000);
    	Reporter.log("Introduction Start Shoping button clicked succefully");
    }
    
//    public void continueShopingButton() throws Exception{
//    	wait.until(ExpectedConditions.elementToBeClickable(btn_continue));
//    	btn_continue.click();
//    	Thread.sleep(3000);
//    }
    
    public void clickOnSelectDept() throws Exception{
    	wait.until(ExpectedConditions.elementToBeClickable(select_dept));
    	select_dept.click();
    	Thread.sleep(3000);
    	
    	Reporter.log("Selected Department tab succefully");
    }
    
 }