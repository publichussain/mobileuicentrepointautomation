package screens;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import configurations.AppFactory;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ProductsScreen extends BasePage{
	
	public ProductsScreen() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@AndroidFindBy(xpath = "(//android.widget.TextView[@content-desc=\"txtPrice, \"])[1]")
    public WebElement item_price;
	
	@AndroidFindBy(xpath = "(//android.widget.TextView[@content-desc=\"txtProductTitle, \"])[1]")
    public WebElement item_title;
	
	@AndroidFindBy(xpath = "(//android.view.ViewGroup[@content-desc=\"imgundefined, \"])[1]/android.view.ViewGroup/android.widget.ImageView")
    public WebElement select_product;
	
	WebDriverWait wait = new WebDriverWait(AppFactory.driver, 10);
	
    public List<Object> getGridProductDetailsAndClick() throws Exception{
    	wait.until(ExpectedConditions.elementToBeClickable(item_price));
    	String gridPage_price = item_price.getText();
    	
    	wait.until(ExpectedConditions.elementToBeClickable(item_title));
    	String gridPage_title = item_title.getText();
    	
    	wait.until(ExpectedConditions.elementToBeClickable(select_product));
    	select_product.click();
    	
    	Thread.sleep(3000);
    	Reporter.log("Product details are colected from the multiple product page and click on product");
    	
    	return Arrays.asList(gridPage_title,gridPage_price);
    }
    
}
