package screens;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import configurations.AppFactory;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ProductDetailsScreen extends BasePage{

	public ProductDetailsScreen() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"txtPrice, \"]")
    public WebElement item_price;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"txtProductTitle, \"]")
    public WebElement item_title;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"ttlAddToBasketButton, \"]")
    public WebElement add_to_bascket;
	
	@AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"btnCheckoutNow, \"]")
    public WebElement checkout;
	
		
	WebDriverWait wait = new WebDriverWait(AppFactory.driver, 10);
	
    public List<Object> getProductDetails() throws Exception{
    	wait.until(ExpectedConditions.elementToBeClickable(item_price));
    	String itemPage_price = item_price.getText();
    	
    	wait.until(ExpectedConditions.elementToBeClickable(item_title));
    	String itemPage_title = item_title.getText();
    	
    	wait.until(ExpectedConditions.elementToBeClickable(add_to_bascket));
    	add_to_bascket.click();
    	
    	Thread.sleep(3000); 	
    	
    	wait.until(ExpectedConditions.elementToBeClickable(checkout));
    	checkout.click();
    	
    	Reporter.log("Product details are colected ,Added to cart and checkout");
    	return Arrays.asList(itemPage_title,itemPage_price);
    }

}
