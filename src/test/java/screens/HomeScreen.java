package screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import configurations.AppFactory;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class HomeScreen extends BasePage{
	
	public HomeScreen() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	@AndroidFindBy(xpath = "(//android.view.ViewGroup[@content-desc=\"toMenuItem, \"])[1]/android.widget.TextView")
    public WebElement select_dept;
	
	@AndroidFindBy(xpath = "(//android.view.ViewGroup[@content-desc=\"toListItemcategory, \"])[6]")
    public WebElement select_category;
	
	

	@AndroidFindBy(xpath = "(//android.view.ViewGroup[@content-desc=\"toListItemcategory, \"])[1]")
    public WebElement select_sub_category;
	
	
	WebDriverWait wait = new WebDriverWait(AppFactory.driver, 10);
	
    public void clickOnSelectDept() throws Exception{
    	wait.until(ExpectedConditions.elementToBeClickable(select_dept));
    	select_dept.click();
    	Thread.sleep(3000);
    	Reporter.log("Selected Department tab succefully");
    }
    
    public void clickOncategory() throws Exception{
    	wait.until(ExpectedConditions.elementToBeClickable(select_category));
    	select_category.click();
    	Thread.sleep(3000);
    	Reporter.log("Selected Department tab succefully");
    }
    
    
    public void clickOnSubcategory() throws Exception{
    	wait.until(ExpectedConditions.elementToBeClickable(select_sub_category));
    	select_sub_category.click();
    	Thread.sleep(3000);
    	Reporter.log("Selected Department tab succefully");
    }

}
