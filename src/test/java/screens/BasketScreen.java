package screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import configurations.AppFactory;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class BasketScreen extends BasePage{

	public BasketScreen() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"viwPicker, \"]/android.widget.Spinner")
    public WebElement drp_qnty;
	
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[2]")
    public WebElement drp_qnty_value;

	@AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"ttlProceedToCheckoutButton, \"]")
    public WebElement btn_proceed_checkout;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"ttlRemoveButton, \"]")
    public WebElement remove_products;
	
	@AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"btnStartShoppingButton, \"]")
    public WebElement start_again;

		
	WebDriverWait wait = new WebDriverWait(AppFactory.driver, 10);
	
    public void addQnty() throws Exception{
    	wait.until(ExpectedConditions.elementToBeClickable(drp_qnty));
    	drp_qnty.click();
    	Thread.sleep(2000);
    	
    	wait.until(ExpectedConditions.elementToBeClickable(drp_qnty_value));
    	drp_qnty_value.click();
    	Thread.sleep(5000);
    	
    	Reporter.log("Increased the quantity in bascket");
    }
    
    public void proceedCheckout() throws Exception{
    	
    	wait.until(ExpectedConditions.elementToBeClickable(btn_proceed_checkout));
    	btn_proceed_checkout.click();
    	Thread.sleep(3000);
    	Reporter.log("proceeded for checkout");
    }
    
    public void removeItems() throws Exception{
    	
    	wait.until(ExpectedConditions.elementToBeClickable(remove_products));
    	remove_products.click();
    	Thread.sleep(2000);
    	Reporter.log("Removed the Items from Basket");
    }
    
    public void starShopingAgain() throws Exception{
    	
    	wait.until(ExpectedConditions.elementToBeClickable(start_again));
    	start_again.click();
    	Thread.sleep(2000);
    	Reporter.log("clicked on Start Shoping");
    }
}
