package screens;

import org.openqa.selenium.support.PageFactory;

import configurations.AppFactory;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BasePage extends AppFactory{
	
	public BasePage() throws Exception{
				
		PageFactory.initElements(new AppiumFieldDecorator(AppFactory.driver), this);		
	}

}
