package tests;

import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import configurations.AppFactory;
import screens.BasketScreen;
import screens.HomeScreen;
import screens.IntroScreens;
import screens.ProductDetailsScreen;
import screens.ProductsScreen;


public class ApplicationFlow extends AppFactory{
	
	List<Object> gridPageDetails;
	List<Object> itemPageDetails;
	
	@BeforeTest
	public void launchApplication() throws IOException {
		AppFactory.Android_LaunchApp();
		Reporter.log("Application Launched Succesfully");
	}
	
	@Test (priority=0)
	public void test_introductoryScreens() throws Exception{
		Reporter.log("Automatino Initiated");
        Thread.sleep(2000);
        IntroScreens intScr = new IntroScreens();
        
        //Introductory screens
        intScr.continueButton();
        intScr.startShopingButton();
        intScr.continueButton();
	}
	
	@Test (priority=1)
	public void test_homeScreen() throws Exception{
		HomeScreen homeScr = new HomeScreen();
		
		//Navigating by selecting category and sub category
        homeScr.clickOnSelectDept();
        homeScr.clickOncategory();
        homeScr.clickOnSubcategory();
	}
	
	@Test (priority=2)
	public void test_productScreen() throws Exception{
		
		// Fetching the product details and adding opening view product screen
		ProductsScreen productScreen = new ProductsScreen();
		gridPageDetails = productScreen.getGridProductDetailsAndClick();
	}
	
	@Test (priority=3)
	public void test_productDetailsScreen() throws Exception{
		
		// Fetching the product details and adding to basket
		ProductDetailsScreen productDetailScreen = new ProductDetailsScreen();
		itemPageDetails = productDetailScreen.getProductDetails();
	}
	
	@Test (priority=4)
	public void test_assertDetails() throws Exception{
		
		 // Assertion logic to verify the title and price of the product
    	    	   	
    	Assert.assertEquals(gridPageDetails.get(0),itemPageDetails.get(0));
		Assert.assertEquals(gridPageDetails.get(1), itemPageDetails.get(1));
		
	}
	
	
	@Test (priority=5)
	public void test_basketScreen() throws Exception{
		
		BasketScreen bascket_screen = new BasketScreen();
		
		//Increasing the quantity and proceeding for checkout
		bascket_screen.addQnty();
//		bascket_screen.proceedCheckout(); // This will take it to signIn page hence not running it.

		//Removing the products and starting again
		bascket_screen.removeItems();
		bascket_screen.starShopingAgain();
	}
	
	
}
