package configurations;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class AppFactory {
	
	public static AndroidDriver driver;
    public static DesiredCapabilities cap;

    public static AndroidDriver Android_LaunchApp() throws MalformedURLException {
        cap = new DesiredCapabilities();
//        
//        File appDir = new File("/MobileUIAutomationCentrePoint/src/test/resources");
//		File app = new File(appDir,"centrepointstores.apk" );
		
        cap.setCapability("platformName", "Android");
        cap.setCapability("deviceName", "UCJV7SHM89WCA6FI");
        cap.setCapability("automationName", "UiAutomator2");
//        cap.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
        cap.setCapability("appPackage", "com.landmarkgroup.centrepointstores");
        cap.setCapability("appActivity", "com.landmarkgroupreactapps.MainActivity");
        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);

        driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		
        System.out.println("Android driver is set");
        return driver;

    }

//    public static void iOS_LaunchApp() throws MalformedURLException {
//        cap = new DesiredCapabilities();
//        cap.setCapability("platformName", "iOS");
//        cap.setCapability("deviceName", "iPhone 11");
//        cap.setCapability("automationName", "XCUITest");
//        cap.setCapability("platformVersion", "13.5");
//        cap.setCapability("bundleId", "xyz");
//        driver = new IOSDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
//        AppDriver.setDriver(driver);
//        System.out.println("iOS driver is set");
//
//    }

    public static void closeApp(){
        driver.quit();
    }


}
